#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* initialisation de la liste_t */
void initialisation (liste_t * liste_t);


/* INSERTION */

/* insertion dans une liste_t vide */
int ins_dans_liste_vide (liste_t * liste_t, char *donnee);

/* insertion au début de la liste_t */
int ins_debut_liste (liste_t * liste_t, char *donnee);

/* insertion Ã a fin de la liste_t */
int ins_fin_liste (liste_t * liste_t, cellule_t * courant, char *donnee);

/* insertition ailleurs */
int ins_liste (liste_t * liste_t, char *donnee, int pos);


/* SUPPRESSION */

int supp_debut (liste_t * liste_t);
int supp_dans_liste (liste_t * liste_t, int pos);


int menu (liste_t *liste_t,int *k);
void affiche (liste_t * liste_t);
void detruire (liste_t * liste_t);