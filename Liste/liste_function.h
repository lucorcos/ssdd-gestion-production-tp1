#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct cellule
{
    char *ligne;
    struct cellule *suivant;
} cellule_t;

typedef struct liste_t
{
    cellule_t *debut;
    cellule_t *fin;
    int taille;
} liste_t;


void initialisation(liste_t *liste_t)
{
    liste_t->debut = NULL;
    liste_t->fin = NULL;
    liste_t->taille = 0;
}

/* insertion dans une liste_t vide */
int ins_dans_liste_vide(liste_t *liste_t, char *ligne)
{
    cellule_t *nouveau_cellule_t;
    if ((nouveau_cellule_t = (cellule_t *)malloc(sizeof(cellule_t))) == NULL)
        return -1;
    if ((nouveau_cellule_t->ligne = (char *)malloc(50 * sizeof(char))) == NULL)
        return -1;
    strcpy(nouveau_cellule_t->ligne, ligne);
    nouveau_cellule_t->suivant = NULL;
    liste_t->debut = nouveau_cellule_t;
    liste_t->fin = nouveau_cellule_t;
    liste_t->taille++;
    return 0;
}

/* insertion au début de la liste */
int ins_debut_liste(liste_t *liste_t, char *ligne)
{
    cellule_t *nouveau_cellule_t;
    if ((nouveau_cellule_t = (cellule_t *)malloc(sizeof(cellule_t))) == NULL)
        return -1;
    if ((nouveau_cellule_t->ligne = (char *)malloc(50 * sizeof(char))) == NULL)
        return -1;
    strcpy(nouveau_cellule_t->ligne, ligne);
    nouveau_cellule_t->suivant = liste_t->debut;
    liste_t->debut = nouveau_cellule_t;
    liste_t->taille++;
    return 0;
}

/*insertion à la fin de la liste */
int ins_fin_liste(liste_t *liste_t, cellule_t *courant, char *ligne)
{
    cellule_t *nouveau_cellule_t;
    if ((nouveau_cellule_t = (cellule_t *)malloc(sizeof(cellule_t))) == NULL)
        return -1;
    if ((nouveau_cellule_t->ligne = (char *)malloc(50 * sizeof(char))) == NULL)
        return -1;
    strcpy(nouveau_cellule_t->ligne, ligne);
    courant->suivant = nouveau_cellule_t;
    nouveau_cellule_t->suivant = NULL;
    liste_t->fin = nouveau_cellule_t;
    liste_t->taille++;
    return 0;
}

/* insertion à la position demandée */
int ins_liste(liste_t *liste_t, char *ligne, int pos)
{
    if (liste_t->taille < 2)
        return -1;
    if (pos < 1 || pos >= liste_t->taille)
        return -1;
    cellule_t *courant;
    cellule_t *nouveau_cellule_t;
    int i;
    if ((nouveau_cellule_t = (cellule_t *)malloc(sizeof(cellule_t))) == NULL)
        return -1;
    if ((nouveau_cellule_t->ligne = (char *)malloc(50 * sizeof(char))) == NULL)
        return -1;
    courant = liste_t->debut;
    for (i = 1; i < pos; ++i)
        courant = courant->suivant;
    if (courant->suivant == NULL)
        return -1;
    strcpy(nouveau_cellule_t->ligne, ligne);
    nouveau_cellule_t->suivant = courant->suivant;
    courant->suivant = nouveau_cellule_t;
    liste_t->taille++;
    return 0;
}

/* suppression au début de la liste */
int supp_debut(liste_t *liste_t)
{
    if (liste_t->taille == 0)
        return -1;
    cellule_t *supp_cellule_t;
    supp_cellule_t = liste_t->debut;
    liste_t->debut = liste_t->debut->suivant;
    if (liste_t->taille == 1)
        liste_t->fin = NULL;
    free(supp_cellule_t->ligne);
    free(supp_cellule_t);
    liste_t->taille--;
    return 0;
}
/* supprimer un cellule après la position demandée */
int supp_dans_liste(liste_t *liste_t, int pos)
{
    if (liste_t->taille <= 1 || pos < 1 || pos >= liste_t->taille)
        return -1;
    int i;
    cellule_t *courant;
    cellule_t *supp_cellule_t;
    courant = liste_t->debut;
    for (i = 1; i < pos; ++i)
        courant = courant->suivant;
    supp_cellule_t = courant->suivant;
    courant->suivant = courant->suivant->suivant;
    if (courant->suivant == NULL)
        liste_t->fin = courant;
    free(supp_cellule_t->ligne);
    free(supp_cellule_t);
    liste_t->taille--;
    return 0;
}

/* affichage de la liste */
void affiche(liste_t *liste_t)
{
    int cmp = 0;
    cellule_t *courant;
    courant = liste_t->debut;
    while (courant != NULL)
    {
        printf ("%p - %s\n", courant, courant->ligne);
		courant = courant->suivant;

    }
}

/* detruire la liste */
void detruire(liste_t *liste_t)
{
    while (liste_t->taille > 0)
        supp_debut(liste_t);
}

int menu(liste_t *liste_t, int *k)
{
    int choix;
    printf("\n********** MENU **********\n");
	if (liste_t->taille == 0){
        printf ("1. Ajout de la 1ère cellule\n");
		printf ("2. Quitter\n");
	} else if(liste_t->taille == 1 || *k == 1){
        printf ("1. Ajout au debut de la liste\n");
		printf ("2. Ajout a la fin de la liste\n");
		printf ("4. Suppression au debut de la liste\n");
		printf ("6. Detruire la liste\n");
		printf ("7. Quitter\n");
	}else {
        printf ("1. Ajout au debut de la liste\n");
		printf ("2. Ajout a la fin de la liste\n");
		printf ("3. Ajout apres la position specifie\n");
		printf ("4. Suppression au debut de la liste\n");
		printf ("5. Suppression apres la position specifie\n");
		printf ("6. Detruire la liste\n");
		printf ("7. Quitter\n");
	}
	printf ("Faites votre choix : ");
	scanf ("%d", &choix);
	getchar();
	if (liste_t->taille == 0 && choix == 2)
		choix = 7;
	return choix;
}