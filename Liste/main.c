#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "liste_function.h"
#include "liste.h"

int main(void)
{
    char choix;
    char *nom;
    liste_t *liste;
    cellule_t *courant;
    if ((liste = (liste_t *)malloc(sizeof(liste_t))) == NULL)
        return -1;
    if ((nom = (char *)malloc(50)) == NULL)
        return -1;
    courant = NULL;
    choix = 'o';
    initialisation(liste);
    int pos, k;
    while (choix != 7)
    {
        choix = menu(liste, &k);
        switch (choix)
        {
        case 1:
            printf("Entrez un element : ");
            scanf("%s", nom);
            getchar();
            if (liste->taille == 0)
                ins_dans_liste_vide(liste, nom);
            else
                ins_debut_liste(liste, nom);
            printf("\nIl y a %d element(s).\nDébut = %s\nFin = %s\n\n", liste->taille,
                   liste->debut->ligne,
                   liste->fin->ligne);
            affiche(liste);
            break;
        case 2:
            printf("\nEntrez un element : ");
            scanf("%s", nom);
            getchar();
            ins_fin_liste(liste, liste->fin, nom);
            printf("\nIl y a %d element(s).\nDébut = %s\nFin = %s\n\n", liste->taille,
                   liste->debut->ligne,
                   liste->fin->ligne);
            affiche(liste);
            break;
        case 3:
            printf("\nEntrez un element : ");
            scanf("%s", nom);
            getchar();
            do
            {
                printf("\nEntrez la position : ");
                scanf("%d", &pos);
            } while (pos < 1 || pos > liste->taille);
            getchar();
            if (liste->taille == 1 || pos == liste->taille)
            {
                k = 1;
                printf("-----------------------------------------------");
                printf(" Insertion echouée. Utilisez le menu {1|2}  \n");
                printf("-----------------------------------------------");
                break;
            }
            ins_liste(liste, nom, pos);
            printf("\nIl y a %d element(s).\nDébut = %s\nFin = %s\n\n", liste->taille,
                   liste->debut->ligne,
                   liste->fin->ligne);
            affiche(liste);
            break;
        case 4:
            supp_debut(liste);
            if (liste->taille != 0)
                printf("\nIl y a %d element(s).\nDébut = %s\nFin = %s\n\n", liste->taille,
                       liste->debut->ligne,
                       liste->fin->ligne);
            else
                printf("\nliste_t vide\n");
            affiche(liste);
            break;
        case 5:
            do
            {
                printf("\nEntrez la position : ");
                scanf("%d", &pos);
            } while (pos < 1 || pos > liste->taille);
            getchar();
            supp_dans_liste(liste, pos);
            if (liste->taille != 0)
                printf("Il y a %d element(s).\nDébut = %s\nFin = %s\n\n", liste->taille,
                       liste->debut->ligne, liste->fin->ligne);
            else
                printf("liste vide");
            affiche(liste);
            break;
        case 6:
            detruire(liste);
            printf("\nLa liste a ete detruite : %d elements\n", liste->taille);
            break;
        }
    }
    return 0;
}