Liste des fichiers avec leurs dépendances et rôles
--------------------------------------------------

    file.c  file.h
    --------------
Gestion de la lecture et de l'écriture du fichier. Permet la création de la matrice et l'export du fichier final.

Liste des fonctions :
FILE *openFile(char *);
double **fillMatrixWithFile(char *, int*, int*);
double **fillMatrix(FILE *, int *, int *);

    list.c  list.h (cell.h)
    -----------------------
Gestion de la création de la liste chainées avec les fonctions usuelles de traitement sur une liste.

Liste des fonctions et structure :
typedef struct listKmin{
    kmin_t firstcell;
    int size;
}listKmin_t;

listKmin_t* creatList();
void insertVal(kmin_t *, double , int , int );
void freeList(kmin_t *);
void showList(kmin_t *, int);
void insertValSort(kmin_t *, double , int , int );
void defineKMin(double **, int , int, int);
void supprFactories(listKmin_t *, int );


Différents test :
------------------

     - Au niveau des cellules :    OK
     
          * Afficher une cellule (la première ? Une en particulier ? Les deux ?)          OK
          * Créer une cellule                                                             OK
          * Supprimer une cellule (impossible de supprimer un cellule qui n'existe pas)   OK
          * Trouver une cellule en fct de l'usine (cas favorable / défavorable)           OK

     - Au niveau des listes : OK
          
          * Affciher la liste (liste vide, un élément ou plus)                                           OK
          * Créer une liste (liste vide, un élément ou plus)                                             OK
          * Supprimer la liste (liste vide, un élément ou plus)                                          OK
          * Supprimer la tête (liste vide, un élément ou plus)                                           OK          
          * Insérer une valeur (cas ou la valeur ou la liste n'est pas correct)                          OK
          * Insérer une valeur dans la lsite triée (cas ou la valeur ou la liste n'est pas correct)      OK
          
          
     - Au niveau des fichiers :
          * Ouvrir le fichier           OK
          * Remplir la matrice avec le fichier (cas : fichier vide, fichier à un élement ou plus (double ou int))  OK
          * Afficher la matrice    OK
          * Supprimer l'usine n (usine présente ou pas)          KO
          * Créer la liste à partir de la matrice (matrice vide, à un élément, à plusieurs)         OK
          (On considère que la matrice est OK car vérifier dans les opérations de matrices
          * Exporter le nouveau fichier      OK
       (optionnel)
          * Vérifier le fichier (avec des char, problèmes n et m au niveau de l'entête)
          

     - Au niveau du programme final :
          * Cas avec k (k < 0, k = 0 ou k incorrect, nombre à virgule, char, ...)
          * Nombre d'éléments de la matrice inférieur à k
          * Matrice avec que des éléments identiques
          * Matrice avec des elemts tous différents
          * Matrice très grande


