#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../inc/file.h"
#include "../inc/list.h"
#include "../inc/test.h"

int main(int argc, char *argv[])
{

    if (argc != 3)
    {
        printf("The number of arguments is incorrect: I need the name of the file and the minimum cost number to fill in. Thank you.\n");
    }
    else
    {
        int k = atoi(argv[2]);
        if (k != 0)
        { //if k is an integer not equal to 0

            double **matrix = fillMatrixWithFile(argv[1]);
            listKmin_t *list = creatList();

            int choice;
            char okay;

            defineList(matrix, k, col, lig, list);
            showList(&(list->firstcell));

            printf("Do you want to delete, from the selections, the occurrences of a factory ? Y/N\n");
            scanf("%c%*c", &okay);
            if (okay == 'Y' || okay == 'y')
            {
                printf("Enter the value of the plant to be deleted\n");
                scanf("%d", &choice);
                supprFactories(list, choice);
                showList(&(list->firstcell));
            }

            printf("Would you like to save the selection ? Y/N\n");
            scanf("%c%*c", &okay);
            if (okay == 'Y' || okay == 'y')
            {
                CsvExport(&(list->firstcell));
                printf("File saved\n");
            }

            printf("Would you like to view the tests performed ? Y/N\n");
            printf("Errors with Valgrind will be generated but they are caused to generate error cases.\n");
            scanf("%c%*c", &okay);
            if (okay == 'Y' || okay == 'y')
            {
                mainTest();
            }
        }
        else
            printf("K must be an integer other than 0, and says this to pass after the filename\n");
    }
    return 0;
}
