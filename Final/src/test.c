#include "../inc/test.h"

/************************************************************************
 * void testShowCell(kmin_t *previous)
 * Test to see if a cell have a good display in the shell. (We simulate 
 * a linked list)
 * Input : cell (pointer on a cell)
 * Output : Nothing (it's just printing)
 * *********************************************************************/
void testShowCell(kmin_t *previous)
{
    int count = 1; // Replace the rank of the real list
    while (previous != NULL)
    {
        showCell(previous, count);
        previous = previous->foll;
        count++;
    }
}

/************************************************************************
 * void testSearchCellByFactory(kmin_t *cell_a, kmin_t * cell_b)
 * Test to see if the iddentification of a factory in a cell works
 * Input : (cells to test)
 *        cell_a (pointer on a cell)
 *        cell_ab(pointer on a cell)
 * Output : Nothing (it's just printing)
 * *********************************************************************/
void testSearchCellByFactory(kmin_t *cell_a, kmin_t *cell_b)
{
    printf("cell_a :\n");
    showCell(cell_a, 2);
    printf("cell_b :\n");
    showCell(cell_b, 3);
    int notOkay = searchCellByFactory(*cell_a, 4);
    int okay = searchCellByFactory(*cell_b, 9);
    printf("Cell_a => %d\tCell_b => %d\n", okay, notOkay); // print 1 0
}

/************************************************************************
 * void testCell()
 * Set of tests on functions concerning cells 
 * Input : Nothing
 * Output : Nothing (it's just printing and call functions)
 * *********************************************************************/
void testCell()
{
    printf("Creating cell in progress...\n");
    printf("4 \"good\" cells were created and 1 \"bad\".\n");
    kmin_t *cell = creatCell();
    kmin_t *cell2 = creatCell();
    kmin_t *cell3 = creatCell();
    kmin_t *cell4 = creatCell();
    kmin_t *cell_ko = NULL;

    //  Initialisation of the cells
    //  We create a "fake" linked list
    cell->foll = cell2;
    cell2->foll = cell3;
    cell2->value = 63;
    cell2->factory = 6;
    cell3->value = 413;
    cell3->factory = 3;
    cell3->foll = cell4;
    cell4->value = 93;
    cell4->factory = 9;
    cell4->foll = cell_ko;

    //  Test on the cells
    printf("Test showCell\n");
    testShowCell(cell); //  Display the "fake" linked list

    printf("\nTest of a suppression of a cell (The second will be deleted).\n");
    supprCell(cell); //  Delete the second cell
    testShowCell(cell);

    printf("\nTest of a suppression of a BAD cell (cell_ko will be deleted ?).\n");
    supprCell(cell4);      //  Try to delete the bad cell
    testShowCell(cell_ko); //  Try to print, but nothing to show
    testShowCell(cell);

    printf("\nTest of a search\n");
    testSearchCellByFactory(cell3, cell4); //  Search factory 4 in cell 3
                                           //  Search factory 9 in cell 4

    //  Memory release
    supprCell(cell);
    supprCell(cell);
    free(cell);
};

/************************************************************************
 * void testShowList()
 * Create a "fake" list and display it
 * Input : Nothing
 * Output : Nothing (it's just printing and call functions)
 * *********************************************************************/
void testShowList()
{
    printf("Creating cell in progress...\n");
    printf("3 \"good\" cells were created and 1 \"bad\".\n");
    kmin_t *cell = creatCell();
    kmin_t *cell2 = creatCell();
    kmin_t *cell3 = creatCell();
    kmin_t *cell_ko = NULL;

    //  Initialisation of the cells
    //  We create a "fake" linked list
    cell->foll = cell2;
    cell2->foll = cell3;
    cell3->foll = cell_ko;
    cell2->value = 63;
    cell2->factory = 6;
    cell3->value = 413;
    cell3->factory = 3;

    //  Printing cell2 an cell3 (cell is a fictive head)
    showList(cell);

    printf("\n");

    //  Trying to print cell_ko
    printf("\nTrying to print an empty list\n");
    showList(cell3);

    //  Memory release
    supprCell(cell);
    supprCell(cell);
    free(cell);
}

/************************************************************************
 * void testList()
 * Set of tests on functions concerning lists 
 * Input : Nothing
 * Output : Nothing (it's just printing and call functions)
 * *********************************************************************/
void testList()
{
    listKmin_t *list = creatList();

    //  Test the display of a list
    printf("List display test (without real list)\n");
    testShowList();

    //  Test the display of a true empty list
    printf("\nTrying to print an empty real list\n");
    showList(&(list->firstcell));

    printf("\nInsertion of values in progress...\n");
    //  Insertion of values (sorted list)
    insertVal(&(list->firstcell), 2.32, 42, 64);
    insertVal(&(list->firstcell), 20.38, 4, 6);
    insertVal(&(list->firstcell), 126, 1, 12);
    insertVal(&(list->firstcell), 200, 7, 19);

    //  Test the display of a true  list
    printf("\nTrying to print a real list\n");
    showList(&(list->firstcell));

    //  Delete the first element of the list
    printf("\nDeleting the first element in progress...\n");
    printf("The new list is :\n");
    supprCell(&(list->firstcell));
    showList(&(list->firstcell));

    //  Insert at the good place an element
    printf("\nSorted insertion in progress...\n");
    insertValSort(&(list->firstcell), 1.23, 16, 8);
    printf("The new list is :\n");
    showList(&(list->firstcell));

    //! Solve Valgring error from here...
    //  Delete the the list
    printf("\nDeleting the list in progress...\n");
    freeList(&(list->firstcell));
    showList(&(list->firstcell));

    //  Impossible to free an empty list without an error :
    //  free(): double free detected in tcache 2
    //  Abandon (core dumped)
    //freeList(&(list->firstcell));

    //  Trying to insert at the good place an element in deleted list
    printf("\nSorted insertion in deleted list in progress...\n");
    insertValSort(&(list->firstcell), 1.23, 16, 8);
    printf("The new list is :\n");
    showList2(&(list->firstcell), 3);
}

/************************************************************************
 * void testFile()
 * Set of tests on functions concerning the files 
 * Input : Nothing
 * Output : Nothing (it's just printing and call functions)
 * *********************************************************************/
void testFile()
{

    int k = 10;                     //  Set the min elements to selec in the file
    listKmin_t *list = creatList(); //  Creation of an empty list
    double **matrix;                //  Création of a pointer for the matrix

    //  Trying to fill a matrix with an empty file
    printf("Trying to fill a matrix with an empty file.\n");
    matrix = fillMatrixWithFile("./data/data0.csv");
    printMatrix(matrix);

    //  Trying to fill a matrix with a little file
    printf("Trying to fill a matrix with a little file.\n");
    matrix = fillMatrixWithFile("./data/data2.csv");
    printMatrix(matrix);

    //  Define the k minimum element in the matrix to the list
    printf("Creating the list with %d min elements from the matrix.\n", k);
    defineList(matrix, k, col, lig, list);
    showList(&(list->firstcell));

    //  Delete the factory 9
    printf("\nDeleting the factory 9 from the list.\n");
    supprFactories(list, 9);
    showList(&(list->firstcell));

    //  Trying to remove a factory that doesn't exist
    printf("\nTrying to remove a factory that doesn't exist.\n");
    supprFactories(list, 4);
    showList(&(list->firstcell));

    //  Export the linked list into a .csv
    printf("\nExport into a .csv in progress...\n");
    CsvExport(&(list->firstcell));

    //  Delete the linked list
    printf("\nDeleting the list in progress...\n");
    freeList(&(list->firstcell));
    showList(&(list->firstcell));

    //  Nothing to export so, no export
    CsvExport(&(list->firstcell));
}

int mainTest()
{
    printf("---------------------------------------------------\n");
    printf("                     Cell tests                     \n");
    printf("---------------------------------------------------\n\n");
    testCell();

    printf("Press any key to continue...\n");
    getchar();

    printf("\n---------------------------------------------------\n");
    printf("                     File tests                     \n");
    printf("---------------------------------------------------\n\n");
    testFile();

    printf("Press any key to continue...\n");
    getchar();

    printf("\n---------------------------------------------------\n");
    printf("                     List tests                     \n");
    printf("---------------------------------------------------\n\n");
    testList();
    return 0;
}