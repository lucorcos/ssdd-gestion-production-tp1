#include "../inc/cell.h"

/************************************************************************
 * kmin_t* creatCell()
 * Allocation for a new cell of the list.
 * Input : Nothing
 * Output : The new cell created.
 * *********************************************************************/
kmin_t *creatCell()
{
    kmin_t *newCell = (kmin_t *)malloc(sizeof(kmin_t)); //allocation of the memory for a new cell
    if (!newCell)
        fprintf(stderr, "Memory allocation not possible for the cell. Error :%s\n", strerror(errno));
    else
    {
        newCell->factory = 0;
        newCell->period = 0;
        newCell->value = 0;
        newCell->foll = NULL;
    }

    return newCell;
}

/************************************************************************
 * void supprCell(kmin_t *previous)
 * Suppression of a cell
 * Input : previous (pointer on the previous element)
 * Output : Nothing
 * *********************************************************************/
void supprCell(kmin_t *previous)
{
    if (previous->foll != NULL)
    {
        kmin_t *toSuppr = previous->foll;
        previous->foll = toSuppr->foll;
        free(toSuppr);
    }
    else
    {
        fprintf(stderr, "Impossible to delete the cell.\n");
    }
}

/************************************************************************
 * void showCell(kmin_t *toshow, int rank)
 * Print every information about an element (a cell)
 * Input : 
 *        toShow : The adress of the element in the list.
 *        rank : its rank in the list.
 * Output : Nothing (just printing)
 * *********************************************************************/
void showCell(kmin_t *toshow, int rank)
{
    if (toshow != NULL)
    {
        printf("Rank %d :\tValue : %.2f\tFactory : %d\tPeriod : %d\n", rank, (*toshow).value, (*toshow).factory, (*toshow).period);
    }
    else
    {
        fprintf(stderr, "Impossible to print the cell.\n");
    }
}

/************************************************************************
 * int searchCellByFactory(kmin_t toLook, int factoryN)
 * Return 1 if the cell has the factory's number given, else 0.
 * Input : 
 *        toLook : pointer on the element.
 *        factoryN : factory's number.
 * Output : 1 if it's from the right factory, else 0.
 * *********************************************************************/
int searchCellByFactory(kmin_t toLook, int factoryN)
{
    int find = 0;
    if (toLook.factory == factoryN)
    {
        find = 1;
    }
    return find;
}