#include "../inc/list.h"

/*creatList : creation of a new list*/
/*input*/
/*output : pointer on the new list creat*/
listKmin_t *creatList()
{
    listKmin_t *list = (listKmin_t *)malloc(sizeof(listKmin_t));
    if (!list)
        fprintf(stderr, "Memory allocation not possible for the list. Error :%s\n", strerror(errno));
    else
    {
        list->size = 0;
        list->firstcell.factory = 0;
        list->firstcell.foll = NULL;
        list->firstcell.period = 0;
        list->firstcell.value = 0;
    }
    return list;
}

/*insertVal : insertion of a value in the linked list*/
/*input : previous (adress of the previous element in the list), val (value), i(values's ligne in the matrix), j(values's collumn in the matrix)*/
/*output*/
void insertVal(kmin_t *previous, double val, int i, int j)
{
    kmin_t *further = creatCell();
    if (!further)
    {
        fprintf(stderr, "Memory allocation not possible. Error : %s\n", strerror(errno));
    }
    else
    {
        further->value = val;
        further->factory = i;
        further->period = j;
        further->foll = previous->foll; //the new cell point on the following cell
        previous->foll = further;       //the previous cell point on the new cell
    }
}

/*freeList : free all the linked list*/
/*input : list ( list of the k min value of the matrix)*/
/*output*/
void freeList(kmin_t *previous)
{
    kmin_t *tmp;

    if (previous != NULL)
    {
        while (previous != NULL)
        {
            tmp = previous->foll;
            free(previous);
            previous = tmp;
        }
    }
    else
    {
        fprintf(stderr, "Impossible to print the list. Error : %s\n", strerror(errno));
    }
}

/*showList : print every list's element */
/*input: previous (adress of the previous element in the list), size (number of element)*/
/*output:*/
void showList2(kmin_t *previous, int size)
{
    int count = 1;
    while (count <= size)
    {
        showCell(previous->foll, count);
        previous = previous->foll;
        count++;
    }
}

void showList(kmin_t *previous)
{
    int count = 1;
    kmin_t *tmp;
    tmp = previous->foll;
    previous = tmp;

    if (previous != NULL)
    {
        while (previous != NULL)
        {
            tmp = previous->foll;
            printf("Rank %d :\tValue : %.2f\tFactory : %d\tPeriod : %d\n", count, (*previous).value, (*previous).factory, (*previous).period);
            previous = tmp;
            count++;
        }
    }
    else
    {
        fprintf(stderr, "Impossible to print the list.\n");
    }
}

/* insertValSort : recursive function used to find the value's good sort place in the kmin list */
/*input: previous (adress of the previous element in the list), val (value), factory (values's ligne in the matrix), period (values's column in the matrix)*/
/*output:*/
void insertValSort(kmin_t *previous, double val, int factory, int period)
{
    if (previous->foll == NULL || previous->foll->value < val)
    {
        insertVal(previous, val, factory, period);
    }
    else
    {
        insertValSort(previous->foll, val, factory, period);
    }
}

/*defineKMin : fill the liste with the K min value of the matrix*/
/*input: matrix(matrix of value from the file), k (number of minimal value wanted by the user), colsize (number of column the matrix), ligsize(number of ligne the matrix) */
/*output:*/
void defineList(double **matrix, int k, int colsize, int ligsize, listKmin_t *list)
{
    int f = 0, p = 0, factories, periods;
    int size = list->size;

    while (size < k)
    {
        if (f < ligsize)
        {

            if (p < colsize)
            {
                insertValSort(&(list->firstcell), matrix[f][p], f, p);
                list->size++;
                p++;
                size++;
            }
            else
            {
                p = 0;
                f++;
            }
        }
    }

    if (p > (colsize - 1))
        p = 0;

    for (factories = f + 1; factories < ligsize; factories++)
    {
        for (periods = p; periods < colsize; periods++)
        {

            if (matrix[factories][periods] < list->firstcell.foll->value)
            {
                insertValSort(&(list->firstcell), matrix[factories][periods], factories, periods);
                supprCell(&(list->firstcell));
            }
        }
    }
}

void supprFactories(listKmin_t *list, int factory)
{
    kmin_t *previous = &(list->firstcell);
    kmin_t *current;
    int find, i;
    int count = 0;

    if (list->size != 0)
    {

        for (i = list->size; i > 0; i--)
        {
            current = previous->foll;
            find = searchCellByFactory(*current, factory);
            if (find == 1)
            {
                supprCell(previous);
                list->size--;
                count++;
            }
            else
            {
                previous = current;
            }
        }
    }
    if (count == 0)
    {
        printf("Cannot find the value to delete.\n");
    }
}