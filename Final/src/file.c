#include "../inc/file.h"

int lig, col;

/*
openFile : open File give in parameters with verification
input : name (name of the file to open)
output : pointer on the file open
*******************************************************/
FILE *openFile(char *name)
{

    FILE *f = fopen(name, "r");
    if (f == NULL)
    {
        perror("Failed to open file\n");
        exit(EXIT_FAILURE);
    }
    return f;
}

/*fillMatrix : fill the matrix with the file's data*/
/*input : file(pointer on the open file)*/
/*output : full matrix*/
double **fillMatrix(FILE *file)
{
    char *curr, *foll;        // pointer of currente and following position in the matrix
    int i = 0, j = 0;         //counter for ligne and column
    int spec_matrix[2] = {0}; //specification of the matrix (number of ligne and column)
    char ligne[TAILLE];       //ligne of the file
    double **matrix;

    while (fgets(ligne, sizeof(ligne), file)) //While there are lignes in the file
    {
        if (i == 0)
        {
            foll = ligne;
            while ((curr = strsep(&foll, ";")) != NULL)
            {
                spec_matrix[j] = atof(curr);
                j++;
            }

            //printf("Number of ligne = %d\nNumber of column = %d\n", spec_matrix[0], spec_matrix[1]);

            matrix = malloc((spec_matrix[0]) * sizeof(double *));
            if (!matrix)
            {
                fprintf(stderr, "Allocation mémoire imposible pour initialiser le tableau. Erreur : %s\n", strerror(errno));
            }
            for (j = 0; j < (spec_matrix[0]); j++)
            {
                matrix[j] = malloc(spec_matrix[1] * sizeof(double));
            }
        }

        else
        {
            j = 0;
            foll = ligne;
            while ((curr = (char *)strsep(&foll, ";")) != NULL)
            {
                matrix[i - 1][j] = atof(curr);
                j++;
            }
        }

        i++;
    }

    lig = spec_matrix[0];
    col = spec_matrix[1];

    return matrix;
}

/* fillMatrixWithFile : Fill a matrix with file's data give by the user */
/* input : nameFile (name of the file wih all datas)*/
/* output : */
double **fillMatrixWithFile(char *nameFile)
{

    double **matrix; //table for the reception of file's data
    FILE *fi;        //pointer on file

    fi = openFile(nameFile);
    matrix = fillMatrix(fi);
    fclose(fi);

    return matrix;
}

void printMatrix(double **matrix)
{
    int i, j;
    for (i = 0; i < lig; i++)
    {
        for (j = 0; j < col; j++)
        {
            printf("%f\t", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    //printf("nb lines = %d\tnb col = %d\n", lig, col);
}

void CsvExport(kmin_t *list)
{

    char date[25];
    char filename[50];

    time_t t = time(NULL);
    kmin_t *tmp;
    struct tm tm = *localtime(&t);
    sprintf(date, "%d-%02d-%02d_%02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

    FILE *fi;

    sprintf(filename, "Factory_export_%s.csv", date);

    tmp = list->foll;
    list = tmp;

    if (list != NULL)
    {
        fi = fopen(filename, "w+");
        fprintf(fi, "Factory;Period;Value\n");

        while (list != NULL)
        {
            tmp = list->foll;

            fprintf(fi, "%d;%d;%.3f\n", (*list).factory, (*list).period, (*list).value);
            list = tmp;
        }
    }
    else
    {
        printf("There is nothig to export.\n");
    }

    fclose(fi);
}
