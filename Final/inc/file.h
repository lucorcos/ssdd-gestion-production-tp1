#ifndef _FILE_H
#define _FILE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "list.h"

#define TAILLE 1024

extern int lig, col;

FILE *openFile(char *);
double **fillMatrixWithFile(char *);
double **fillMatrix(FILE *);
void printMatrix(double **matrix);
void CsvExport(kmin_t *list);

#endif