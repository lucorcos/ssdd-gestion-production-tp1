#ifndef _LIST_H
#define _LIST_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include "cell.h"

typedef struct listKmin
{
    kmin_t firstcell;
    int size;
} listKmin_t;

listKmin_t *creatList();
void insertVal(kmin_t *, double, int, int);
void freeList(kmin_t *);
void showList2(kmin_t *, int);
void showList(kmin_t *previous);
void insertValSort(kmin_t *, double, int, int);
void defineList(double **matrix, int k, int colsize, int ligsize, listKmin_t *list);
void supprFactories(listKmin_t *, int);

#endif