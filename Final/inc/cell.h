#ifndef _CELL_H
#define _CELL_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>

typedef struct kmin
{
    double value;
    int factory;
    int period;
    struct kmin *foll;
} kmin_t;

kmin_t *creatCell();
void supprCell(kmin_t *);
void showCell(kmin_t *, int);
int searchCellByFactory(kmin_t, int);

#endif