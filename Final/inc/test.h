#ifndef _TEST_H
#define _TEST_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>

#include "cell.h"
#include "list.h"
#include "file.h"

void testShowCell(kmin_t *previous);
void testSearchCellByFactory(kmin_t *cell_a, kmin_t *cell_b);
void testCell();
void testShowList();
void testList();
void testFile();
int mainTest();

#endif